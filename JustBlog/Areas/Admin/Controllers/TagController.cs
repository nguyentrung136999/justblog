﻿using JustBlog.Application.Tags;
using JustBlog.ViewModels.Tags;
using Microsoft.AspNetCore.Mvc;

namespace JustBlog.Areas.Admin.Controllers
{
    public class TagController : BaseController
    {
        private readonly ITagService tagService;

        public TagController(ITagService tagService)
        {
            this.tagService = tagService;
        }

        // GET: TagController
        public ActionResult Index()
        {
            var tags = this.tagService.GetAll();
            return View(tags);
        }

        // GET: TagController/Details/5
        public ActionResult Details(int id)
        {
            var tag = this.tagService.GetById(id);
            return View(tag);
        }

        // GET: TagController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TagController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TagVm tagVm)
        {
            if (!ModelState.IsValid)
            {
                return View(tagVm);
            }

            var result = this.tagService.Create(tagVm);
            if (result)
            {
                return RedirectToAction("Index");
            }

            return View(tagVm);
        }

        // GET: TagController/Edit/5
        public ActionResult Edit(int id)
        {
            var tag = this.tagService.GetById(id);
            return View(tag);
        }

        // POST: TagController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, TagVm tagVm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(tagVm);
                }
                this.tagService.Update(tagVm);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TagController/Delete/5
        public ActionResult Delete(int id)
        {
            var tag = this.tagService.GetById(id);
            return View(tag);
        }

        // POST: TagController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, TagVm tagVm)
        {
            try
            {
                this.tagService.Delete(id: id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}