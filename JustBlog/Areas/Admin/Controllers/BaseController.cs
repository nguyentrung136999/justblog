﻿using Microsoft.AspNetCore.Mvc;

namespace JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class BaseController : Controller
    {
    }
}