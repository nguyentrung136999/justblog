﻿using JustBlog.Application.Categories;
using JustBlog.ViewModels.Categories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace JustBlog.Areas.Admin.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly ILogger<CategoryController> logger;
        private readonly ICategoryService categoryService;

        public CategoryController(ILogger<CategoryController> logger, ICategoryService categoryService)
        {
            this.logger = logger;
            this.categoryService = categoryService;
        }

        // GET: CategoryController
        public ActionResult Index()
        {
            var catagories = this.categoryService.GetAll();
            return View(catagories);
        }

        // GET: CategoryController/Details/5
        public ActionResult Details(int id)
        {
            var category = this.categoryService.GetById(id);
            return View(category);
        }

        // GET: CategoryController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CategoryController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryVm categoryVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["Fail"] = "Tạo mới thất bại";
                return View(categoryVm);
            }
            var isSuccess = this.categoryService.Create(categoryVm);
            if (isSuccess)
            {
                TempData["Success"] = "Tạo mới thành công";
                return RedirectToAction("Index", "Category");
            }
            return View(categoryVm);
        }

        // GET: CategoryController/Edit/5
        public ActionResult Edit(int id)
        {
            var category = this.categoryService.GetById(id);
            return View(category);
        }

        // POST: CategoryController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoryVm categoryVm)
        {
            this.categoryService.Update(categoryVm);
            return RedirectToAction("Index", "Category");
        }

        // GET: CategoryController/Delete/5
        public ActionResult Delete(int id)
        {
            var category = this.categoryService.GetById(id);
            return View(category);
        }

        // POST: CategoryController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            this.categoryService.Delete(id: id);
            return RedirectToAction("Index", "Category");
        }
    }
}