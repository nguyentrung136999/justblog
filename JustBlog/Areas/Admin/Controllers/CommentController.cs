﻿using JustBlog.Application.Comments;
using JustBlog.ViewModels.Comments;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.Areas.Admin.Controllers
{
    public class CommentController : BaseController
    {
        private readonly ILogger<CategoryController> logger;
        private readonly ICommentService commentService;

        public CommentController(ILogger<CategoryController> logger, ICommentService commentService)
        {
            this.logger = logger;
            this.commentService = commentService;
        }

        // GET: CommentController
        public ActionResult Index()
        {
            var comments = this.commentService.GetAll();
            return View(comments);
        }

        // GET: CommentController/Details/5
        public ActionResult Details(int id)
        {
            var comment = this.commentService.GetById(id);
            return View(comment);
        }

        // GET: CommentController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CommentController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CommentVm commentVm)
        {
            try
            {
                this.commentService.Create(commentVm);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CommentController/Edit/5
        public ActionResult Edit(int id)
        {
            var comment = this.commentService.GetById(id);
            return View(comment);
        }

        // POST: CommentController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, CommentVm commentVm)
        {
            try
            {
                this.commentService.Update(commentVm);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CommentController/Delete/5
        public ActionResult Delete(int id)
        {
            var comment = this.commentService.GetById(id);
            return View(comment);
        }

        // POST: CommentController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                this.commentService.Delete(id: id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
