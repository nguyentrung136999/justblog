﻿using JustBlog.Application.Posts;
using JustBlog.Common.Constraints;
using JustBlog.Models.Entities;
using JustBlog.ViewModels.Posts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace JustBlog.Areas.Admin.Controllers
{
    public class PostController : BaseController
    {
        private readonly IPostService postService;

        public PostController(IPostService postService)
        {
            this.postService = postService;
        }

        // GET: PostController
        public IActionResult Index(string keyword=null,string searchBy = Constraints.Search.Title,int pageIndex = 1, int pageSize = 3, string typeOfSoft = Constraints.Soft.ASC)
        {
            if (TempData["InforCreate"] != null)
            {
                TempData["Success"] = TempData["InforCreate"];
            }
            Func<Post, bool> filter = null;

            if (!string.IsNullOrEmpty(keyword))
            {
                ViewBag.Keyword = keyword;
                ViewBag.SearchBy = searchBy;
                keyword = keyword.ToLower();
                if (searchBy == Constraints.Search.Title)
                {
                    filter = x => x.Title.ToLower().Contains(keyword);
                }
                else
                {
                    filter = x => x.Content.ToLower().Contains(keyword);
                }
            }
            ViewBag.TypeOfSoft = typeOfSoft;
            var postPaged = this.postService.GetPaging(filter: filter, searchBy: searchBy, pageIndex: pageIndex, pageSize: pageSize, typeOfSoft: typeOfSoft);
            return View(postPaged);
        }

        // GET: PostController/Details/5
        public ActionResult Details(int id)
        {
            var post = this.postService.GetById(id);
            return View(post);
        }

        // GET: PostController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PostController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreatePostVm createPostVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforCreate"] = "Tạo mới thất bại";
                return View(createPostVm);
            }
            var isSuccess = this.postService.Create(createPostVm);
            if (isSuccess)
            {
                TempData["InforCreate"] = "Tạo mới thành công";
                return RedirectToAction("Index", "Post");
            }
            return View();
        }

        // GET: PostController/Edit/5
        public ActionResult Edit(int id)
        {
            var post = this.postService.GetById(id);
            return View(post);
        }

        // POST: PostController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, PostVm postVm)
        {
            if (!ModelState.IsValid)
            {
                TempData["InforUpdate"] = "Cập nhật thất bại";
                return View(postVm);
            }
            this.postService.Update(postVm);
            return RedirectToAction("Index", "Post");
        }

        // GET: PostController/Delete/5
        public ActionResult Delete(int id)
        {
            var post = this.postService.GetById(id);
            return View(post);
        }

        // POST: PostController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            this.postService.Delete(id: id);
            return RedirectToAction("Index");
        }
    }
}