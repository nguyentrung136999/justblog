﻿using JustBlog.Application.Tags;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.Controllers
{
    public class TagController : Controller
    {
        private readonly ILogger<TagController> logger;
        private readonly ITagService tagService;

        public TagController(ILogger<TagController> logger, ITagService tagService)
        {
            this.logger = logger;
            this.tagService =tagService;
        }
        public IActionResult Details(string url)
        {
            var tag=this.tagService.GetByUrlSlug(url);
            return View(tag);
        }
    }
}
