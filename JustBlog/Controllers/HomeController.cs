﻿using JustBlog.Application.Posts;
using JustBlog.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IPostService _categoryService;

        public HomeController(ILogger<HomeController> logger,IPostService postService)
        {
            _logger = logger;
            _categoryService = postService;
        }

        public IActionResult Index()
        {
            var posts = _categoryService.GetAll().Take(10);
            return View(posts);
        }
        public IActionResult Details(int id)
        {
            var post = _categoryService.GetById(id);
            return View(post);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
