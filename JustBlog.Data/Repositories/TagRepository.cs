﻿using JustBlog.Data.Helpers;
using JustBlog.Data.Infrastructures;
using JustBlog.Data.IRepositories;
using JustBlog.Models.Entities;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Data.Repositories
{
    public class TagRepository : GenericRepository<Tag, int>, ITagRepository
    {
        public TagRepository(JustBlogDbContext context) : base(context)
        {
        }

        public List<int> AddTagByString(string tagNames)
        {
            var tags = tagNames.Split(';');
            foreach (var tag in tags)
            {
                var tagExisting = this.DbSet.Where(x => x.Name.ToLower().Equals(tag.ToLower())).FirstOrDefault();
                if (tagExisting == null)
                {
                    var newTag = new Tag()
                    {
                        Name = tag,
                        UrlSlug = UrlHepler.FrientlyUrl(tag)
                    };

                    this.DbSet.Add(newTag);
                }
            }
            this.Context.SaveChanges();
            // get tag added
            var tagIds = new List<int>();
            foreach (var tag in tags)
            {
                var tagExisting = this.DbSet.Where(x => x.Name.ToLower().Equals(tag.ToLower())).FirstOrDefault();
                if (tagExisting != null)
                    tagIds.Add(tagExisting.Id);
            }
            return tagIds;
        }

        public Tag GetTagByUrlSlug(string url)
        {
            return this.DbSet.Where(x => x.UrlSlug == url).FirstOrDefault();
        }
    }
}