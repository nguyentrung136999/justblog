﻿using JustBlog.Data.Infrastructures;
using JustBlog.Data.IRepositories;
using JustBlog.Models.Entities;

namespace JustBlog.Data.Repositories
{
    public class CategoryRepository : GenericRepository<Category, int>, ICategoryRepository
    {
        public CategoryRepository(JustBlogDbContext context):base(context)
        {
            
        }

        public void OtherMethodForCategory()
        {
           
        }
    }
}