﻿using JustBlog.Data.Infrastructures;
using JustBlog.Data.IRepositories;
using JustBlog.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Data.Repositories
{
    public class PostRepository : GenericRepository<Post, int>, IPostRepository
    {
        public PostRepository(JustBlogDbContext context) : base(context)
        {
            
        }

        IEnumerable<Post> IPostRepository.GetLastestPost(int size)
        {
            return this.Context.Posts.OrderByDescending(x=>x.CreatedOn).Take(size);
        }
    }
}