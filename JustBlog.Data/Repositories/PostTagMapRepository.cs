﻿using JustBlog.Data.Infrastructures;
using JustBlog.Data.IRepositories;
using JustBlog.Models.Entities;

namespace JustBlog.Data.Repositories
{
    public class PostTagMapRepository : GenericRepository<PostTagMap, int>, IPostTagMapRepository
    {
        public PostTagMapRepository(JustBlogDbContext context) : base(context)
        {
        }
    }
}