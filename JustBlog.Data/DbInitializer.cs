﻿using JustBlog.Models.Entities;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Data
{
    public class DbInitializer
    {
        private readonly JustBlogDbContext _context;

        public DbInitializer(JustBlogDbContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            _context.Database.EnsureCreated();

            if (_context.Categories.Any())
            {
                return;
            }

            var categories = new List<Category>()
            {
                new Category(){Name="Category 1",},
                new Category(){Name="Category 2"},
                new Category(){Name="Category 3"},
            };

            var posts = new List<Post>()
            {
                new Post(){Title="Post 1", Category=categories[0]},
                new Post(){Title="Post 2",Category=categories[0]},
                new Post(){Title="Post 3",Category=categories[1]},
                new Post(){Title="Post 4",Category=categories[2]},
            };

            _context.Posts.AddRange(posts);
            _context.SaveChanges();
        }
    }
}