﻿using JustBlog.Models.BaseEntity;
using JustBlog.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace JustBlog.Data
{
    public class JustBlogDbContext : DbContext
    {
        public JustBlogDbContext()
        {

        }

        public JustBlogDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<PostTagMap> PostTagMaps { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<Comment> Comments { get; set; }

       // public DbSet<Role> Roles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=JustBlogDB;Trusted_Connection=True;");
                optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=JustBlogDB;Trusted_Connection=True;MultipleActiveResultSets=true");
                optionsBuilder.LogTo(Console.WriteLine);
            }
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(category =>
            {
                category.Property(c => c.Name).IsRequired();
                category.HasKey(x => x.Id);
            });

            modelBuilder.Entity<Post>(post =>
            {
                post.HasKey(x => x.Id);
                post.Property(x => x.Title).IsRequired().HasMaxLength(200);

                post.HasOne(p => p.Category)
                    .WithMany(c => c.Posts)
                    .HasForeignKey(p => p.CategoryId);
            });

            modelBuilder.Entity<Tag>(tag =>
            {
                tag.HasKey(x => x.Id);
            });

            modelBuilder.Entity<PostTagMap>(postTag =>
            {
                postTag.HasKey(x => new { x.PostId, x.TagId });
                postTag.HasOne(pt => pt.Tag)
                        .WithMany(t => t.PostTagMaps)
                        .HasForeignKey(pt => pt.TagId);

                postTag.HasOne(pt => pt.Post)
                        .WithMany(p => p.PostTagMaps)
                        .HasForeignKey(pt => pt.PostId);
            });

            modelBuilder.Entity<Comment>(comment =>
            {
                comment.HasKey(x => x.Id);

                comment.HasOne(c => c.Post)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(p => p.PostId);
            });

                  

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            BeforSaveChanges();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            BeforSaveChanges();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void BeforSaveChanges()
        {
            var entities = ChangeTracker.Entries();
            foreach (var entity in entities)
            {
                var now = DateTime.UtcNow;
                if (entity.Entity is IEntityBase<int> asEntity)
                {
                    if (entity.State == EntityState.Added)
                    {
                        asEntity.CreatedOn = now;
                        asEntity.UpdatedOn = now;
                    }
                    if (entity.State == EntityState.Modified)
                    {
                        asEntity.UpdatedOn = now;
                    }
                }
            }
        }
    }
}