﻿using JustBlog.Data.Infrastructures;
using JustBlog.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Data.IRepositories
{
    public interface IPostTagMapRepository : IGenericRepository<PostTagMap, int>
    {
    }
}
