﻿using JustBlog.Data.Infrastructures;
using JustBlog.Models.Entities;
using System.Collections;
using System.Collections.Generic;

namespace JustBlog.Data.IRepositories
{
    public interface IPostRepository : IGenericRepository<Post, int>
    {
        IEnumerable<Post> GetLastestPost(int size);
    }
}