﻿using JustBlog.Data.Infrastructures;
using JustBlog.Models.Entities;

namespace JustBlog.Data.IRepositories
{
    public interface ICommentRepository : IGenericRepository<Comment, int>
    {
    }
}