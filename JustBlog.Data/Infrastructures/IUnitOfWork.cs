﻿using JustBlog.Data.IRepositories;
using System;
using System.Threading.Tasks;

namespace JustBlog.Data.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository CategoryRepository { get; }

        IPostRepository PostRepository { get; }

        ITagRepository TagRepository { get; }

        IPostTagMapRepository PostTagMapRepository { get; }

        ICommentRepository CommentRepository { get; }

        JustBlogDbContext JustBlogDbContext { get; }

        int SaveChanges();

        Task<int> SaveChangesAsync();
    }
}