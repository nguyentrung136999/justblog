﻿using JustBlog.Models.BaseEntity;
using JustBlog.Models.Status;
using JustBlog.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.Data.Infrastructures
{
    public abstract class GenericRepository<TEntity, TKey> : IGenericRepository<TEntity, TKey>
         where TEntity : class, IEntityBase<TKey>
         where TKey : struct
    {
        protected readonly JustBlogDbContext Context;

        protected DbSet<TEntity> DbSet;

        public GenericRepository(JustBlogDbContext context)
        {
            this.Context = context;
            this.DbSet = this.Context.Set<TEntity>();
        }

        public virtual void Add(TEntity entity)
        {
            this.DbSet.Add(entity);
        }

        public virtual async Task AddAsync(TEntity entity)
        {
            await this.DbSet.AddAsync(entity);
        }

        public virtual void AddRange(IEnumerable<TEntity> entities)
        {
            this.DbSet.AddRange(entities);
        }

      
        public virtual async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            await this.DbSet.AddRangeAsync(entities);
        }
      
        public virtual int Count(bool isDeleted = false)
        {
            if (isDeleted == false) return this.DbSet.Count(x => x.IsDeleted == !DeleteStatus.IsDeleted);
            return this.DbSet.Count();
        }

        public virtual async Task<int> CountAsync(bool isDeleted = false)
        {
            if (isDeleted == false) 
                return await this.DbSet.CountAsync(x => x.IsDeleted == !DeleteStatus.IsDeleted);
            return await this.DbSet.CountAsync();
        }

        public virtual void Delete(bool isDeleted = false, params object[] keyValues)
        {
           var entityExisting = this.DbSet.Find(keyValues);
            if (entityExisting != null)
            {
                if (isDeleted == false)
                {
                    entityExisting.IsDeleted = DeleteStatus.IsDeleted;
                    this.Context.Entry<TEntity>(entityExisting).State = EntityState.Modified;
                   
                    return;
                }
                this.DbSet.Remove(entityExisting);
            }
            throw new ArgumentNullException($"{string.Join(";", keyValues)} was not found in the {typeof(TEntity)}");
        }

       
        public virtual void Delete(TEntity entity, bool isDeleted = false)
        {
            var entityExisting = this.DbSet.FirstOrDefault(x => x.Id.Equals(entity.Id));
            if (entityExisting != null)
            {
                if (isDeleted == false)
                {
                    entityExisting.IsDeleted = DeleteStatus.IsDeleted;
                    this.Context.Entry(entityExisting).State = EntityState.Modified;
                    return;
                }
                this.DbSet.Remove(entityExisting);
            }
            throw new ArgumentNullException($"{entity.Id} was not found in the {typeof(TEntity)}");
        }

     
        public virtual async Task DeleteAsync(TEntity entity, bool isDeleted = false)
        {
            var entityExisting = await this.DbSet.FirstOrDefaultAsync(x => x.Id.Equals(entity.Id));
            if (entityExisting != null)
            {
                if (isDeleted == false)
                {
                    entityExisting.IsDeleted = DeleteStatus.IsDeleted;
                    this.Context.Entry(entityExisting).State = EntityState.Modified;
                    return;
                }
                this.DbSet.Remove(entityExisting);
            }
            throw new ArgumentNullException($"{entity.Id} was not found in the {typeof(TEntity)}");
        }

       
        public virtual async Task DeleteAsync(bool isDeleted = false, params object[] keyValues)
        {
            var entityExisting = await this.DbSet.FindAsync(keyValues);
            if (entityExisting != null)
            {
                if (isDeleted == false)
                {
                    entityExisting.IsDeleted = DeleteStatus.IsDeleted;
                    this.Context.Entry(entityExisting).State = EntityState.Modified;
                    return;
                }
                this.DbSet.Remove(entityExisting);
            }
            throw new ArgumentNullException($"{string.Join(";", keyValues)} was not found in the {typeof(TEntity)}");
        }

       
        public virtual void DeleteByCondition(Func<TEntity, bool> condition, bool isDeleted = false)
        {
            var query = this.DbSet.Where(condition);
            foreach (var entity in query)
            {
                this.Delete(entity, isDeleted);
            }
        }

      
        public virtual async Task DeleteByConditionAsync(Func<TEntity, bool> condition, bool isDeleted = false)
        {
            var query = this.DbSet.Where(condition);
            foreach (var entity in query)
            {
                await this.DeleteAsync(entity, isDeleted);
            }
        }

       
        public IEnumerable<TEntity> Find(Func<TEntity, bool> condition, bool isDeleted = false)
        {
            var query = this.DbSet.Where(condition);
            if (isDeleted == false)
            {
                return query.Where(x => x.IsDeleted == !DeleteStatus.IsDeleted);
            }
            return query;
        }

        public Task<IEnumerable<TEntity>> FindAsync(Func<TEntity, bool> condition, bool isDeleted = false)
        {
            throw new NotImplementedException();
        }
       
        public virtual IEnumerable<TEntity> GetAll(bool isDeleted = false)
        {
            if (isDeleted == false)
            {
                return this.DbSet.Where(x => x.IsDeleted != DeleteStatus.IsDeleted);
            }
            return this.DbSet.AsEnumerable();
        }
       
        public virtual async Task<IEnumerable<TEntity>> GetAllAsync(bool isDeleted = false)
        {
            if (isDeleted == false)
            {
                return await this.DbSet.Where(x => x.IsDeleted != DeleteStatus.IsDeleted).ToListAsync();
            }
            return await this.DbSet.ToListAsync();
        }
     
        public TEntity GetById(params object[] keyValues)
        {
            return this.DbSet.Find(keyValues);
        }

      
        public virtual async Task<TEntity> GetByIdAsync(params object[] keyValues)
        {
            return await this.DbSet.FindAsync(keyValues);
        }

        public PagedVm<TEntity> GetPaging(Func<TEntity, bool> filter = null, Func<IEnumerable<TEntity>, IOrderedEnumerable<TEntity>> orderBy = null, bool isDeleted = false, string keyword = null, int pageIndex = 1, int pageSize = 10)
        {
            var query = this.DbSet.AsEnumerable();
            if (filter != null)
            {
                query = query.Where(filter);
            }
            if (isDeleted == false)
            {
                query = query.Where(x => x.IsDeleted != DeleteStatus.IsDeleted);
            }

            int totalRows = query.Count();
            int totalPage = (int)Math.Ceiling((decimal)totalRows / pageSize);

            if (orderBy != null)
            {
                query = orderBy(query);
            }
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);


            return new PagedVm<TEntity>(query, pageIndex, pageSize, totalPage);
        }

        public void Update(TEntity entity)
        {
            this.DbSet.Update(entity);
        }
    }
}