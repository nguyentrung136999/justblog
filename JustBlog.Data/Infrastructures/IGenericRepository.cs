﻿using JustBlog.Models.BaseEntity;
using JustBlog.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JustBlog.Data.Infrastructures
{
    public interface IGenericRepository<TEntity, TKey>
        where TEntity : class, IEntityBase<TKey>
        where TKey : struct
    {
        IEnumerable<TEntity> GetAll(bool isDeleted = false);

        TEntity GetById(params object[] keyValues);

        Task<TEntity> GetByIdAsync(params object[] keyValues);

        Task<IEnumerable<TEntity>> GetAllAsync(bool isDeleted = false);

        void Add(TEntity entity);

        Task AddAsync(TEntity entity);

        void AddRange(IEnumerable<TEntity> entities);

        Task AddRangeAsync(IEnumerable<TEntity> entities);

        void Update(TEntity entity);

        void Delete(bool isDeleted = false, params object[] keyValues);

        void Delete(TEntity entity, bool isDeleted = false);

        void DeleteByCondition(Func<TEntity, bool> condition, bool isDeleted = false);

        Task DeleteAsync(TEntity entity, bool isDeleted=false);

        Task DeleteAsync(bool isDeleted = false, params object[] keyValues);

        Task DeleteByConditionAsync(Func<TEntity, bool> condition, bool isDeleted = false);

        IEnumerable<TEntity> Find(Func<TEntity, bool> condition, bool isDeleted=false);

        Task<IEnumerable<TEntity>> FindAsync(Func<TEntity, bool> condition, bool isDeleted = false);

        int Count(bool isDeleted = false);

        Task<int> CountAsync(bool isDeleted = false);

        PagedVm<TEntity> GetPaging(Func<TEntity, bool> filter = null, Func<IEnumerable<TEntity>, IOrderedEnumerable<TEntity>> orderBy = null, bool isDeleted = false, string keyword = null, int pageIndex = 1, int pageSize = 10);

    }
}