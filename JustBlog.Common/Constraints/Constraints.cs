﻿namespace JustBlog.Common.Constraints
{
    public static class Constraints
    {
        public const int PageSize = 2;

        public class Search
        {
            public const string Title = "Title";
            public const string Content = "Content";
        }

        public class Soft
        {
            public const string ASC = "ASC";
            public const string DESC = "DESC";
        }
    }
}