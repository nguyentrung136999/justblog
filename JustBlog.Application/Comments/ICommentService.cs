﻿using JustBlog.Models.Entities;
using JustBlog.ViewModels.Comments;
using System.Collections.Generic;

namespace JustBlog.Application.Comments
{
    public interface ICommentService
    {
        IEnumerable<CommentVm> GetAll(bool isDeleted = false);

        bool Create(CommentVm commentVm);

        CommentVm GetById(int id);

        bool Update(CommentVm commentVm);

        bool Delete(int id);
    }
}