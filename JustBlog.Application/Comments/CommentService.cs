﻿using JustBlog.Data.Infrastructures;
using JustBlog.Models.Entities;
using JustBlog.ViewModels.Comments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Application.Comments
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork unitOfWork;
        public CommentService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }    
        public bool Create(CommentVm commentVm)
        {
            try
            {
                var cmt = new Comment()
                {
                    Id = commentVm.Id,
                    Name = commentVm.Name,
                    IsDeleted = commentVm.IsDeleted,
                    Email = commentVm.Email,
                    PostId = commentVm.PostId,
                    CommentHeader = commentVm.CommentHeader,
                    CommentText=commentVm.CommentText,
                    CommentTime=commentVm.CommentTime,
                    CreatedOn = commentVm.CreatedOn,
                    UpdatedOn = commentVm.UpdatedOn
                };

                this.unitOfWork.CommentRepository.Add(cmt);
                this.unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't Create a new Comment");
            }
        }

        public bool Delete(int id)
        {
            try
            {
                this.unitOfWork.CategoryRepository.Delete(keyValues: id);
                return this.unitOfWork.SaveChanges() > 0;
            }
            catch (Exception)
            {
                throw new Exception("Delete fail");
            }
        }

        public IEnumerable<CommentVm> GetAll(bool isDeleted = false)
        {
            try
            {
                var comments = this.unitOfWork.CommentRepository.GetAll(isDeleted);
                List<CommentVm> commentVms = new List<CommentVm>();
                foreach (var comment in comments)
                {
                    var cmt = new CommentVm()
                    {
                        Id = comment.Id,
                        Name = comment.Name,
                        IsDeleted = comment.IsDeleted,
                        Email = comment.Email,
                        PostId = comment.PostId,
                        CommentHeader = comment.CommentHeader,
                        CommentText = comment.CommentText,
                        CommentTime = comment.CommentTime,
                        CreatedOn = comment.CreatedOn,
                        UpdatedOn = comment.UpdatedOn
                    };
                    commentVms.Add(cmt);
                }
                return commentVms;
            }
            catch (Exception)
            {
                throw new Exception("Fail get all comment");
            }
        }

        public CommentVm GetById(int id)
        {
            try
            {
                var comment = this.unitOfWork.CommentRepository.GetById(id);
                var commentVm = new CommentVm()
                    {
                        Id = comment.Id,
                        Name = comment.Name,
                        IsDeleted = comment.IsDeleted,
                        Email = comment.Email,
                        PostId = comment.PostId,
                        CommentHeader = comment.CommentHeader,
                        CommentText = comment.CommentText,
                        CommentTime = comment.CommentTime,
                        CreatedOn = comment.CreatedOn,
                        UpdatedOn = comment.UpdatedOn
                    };
                
                return commentVm;
            }
            catch (Exception)
            {
                throw new Exception("Fail find comment");
            }
        }

        public bool Update(CommentVm commentVm)
        {
            try
            {
                var comment = this.unitOfWork.CommentRepository.Find(x => x.Id == commentVm.Id).FirstOrDefault();

                comment.Name = commentVm.Name;
                comment.Email = commentVm.Email;
                comment.CommentHeader = commentVm.CommentHeader;
                comment.CommentText = commentVm.CommentText;
                comment.CommentTime = commentVm.CommentTime;
                  
                this.unitOfWork.CommentRepository.Update(comment);
                this.unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't update this Comment");
            }
        }
    }
}
