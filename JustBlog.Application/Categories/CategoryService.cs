﻿using JustBlog.Data.Infrastructures;
using JustBlog.Models.Entities;
using JustBlog.ViewModels.Categories;
using System;
using System.Collections.Generic;

namespace JustBlog.Application.Categories
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork unitOfWork;

        public CategoryService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool Create(CategoryVm categoryVm)
        {
            try
            {
                var cate = new Category()
                {
                    Id = categoryVm.Id,
                    Name = categoryVm.Name,
                    IsDeleted = categoryVm.IsDeleted,
                    UrlSlug = categoryVm.UrlSlug,
                    Description = categoryVm.Description,
                    CreatedOn = categoryVm.CreatedOn,
                    UpdatedOn = categoryVm.UpdatedOn
                };

                this.unitOfWork.CategoryRepository.Add(cate);
                this.unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't Create a new Category");
            }
        }

        public bool Delete(int id)
        {
            try
            {
                this.unitOfWork.CategoryRepository.Delete(keyValues: id);
                return this.unitOfWork.SaveChanges()>0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IEnumerable<CategoryVm> GetAll(bool isDeleted = false)
        {
            var categories = this.unitOfWork.CategoryRepository.GetAll(isDeleted);
            var cagoryVms = new List<CategoryVm>();
            foreach (var category in categories)
            {
                var cateVm = new CategoryVm()
                {
                    Id = category.Id,
                    Name = category.Name,
                    IsDeleted = category.IsDeleted,
                    UrlSlug = category.UrlSlug,
                    Description = category.Description,
                    CreatedOn = category.CreatedOn,
                    UpdatedOn = category.UpdatedOn
                };
                cagoryVms.Add(cateVm);
            }
            return cagoryVms;
        }

        public CategoryVm GetById(int id)
        {
            var category = this.unitOfWork.CategoryRepository.GetById(id);

            var catagoryVm = new CategoryVm()
            {
                Id = category.Id,
                Name = category.Name,
                IsDeleted = category.IsDeleted,
                UrlSlug = category.UrlSlug,
                Description = category.Description,
                CreatedOn = category.CreatedOn,
                UpdatedOn = category.UpdatedOn
            };
            return catagoryVm;
        }

        public bool Update(CategoryVm categoryVm)
        {
            try
            {
                var category = this.unitOfWork.CategoryRepository.GetById(categoryVm.Id);
                category.Name = categoryVm.Name;
                category.UrlSlug = categoryVm.UrlSlug;
                category.Description = categoryVm.Description;

                this.unitOfWork.CategoryRepository.Update(category);
                this.unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't update this Category");
            }
        }
    }
}