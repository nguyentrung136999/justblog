﻿using JustBlog.ViewModels.Tags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Application.Tags
{
    public interface ITagService
    {
        IEnumerable<TagVm> GetAll(bool isDeleted = false);

        bool Create(TagVm tagVm);

        TagVm GetById(int id);

        bool Update(TagVm tagVm);

        bool Delete(int id);

        TagVm GetByUrlSlug(string urlSlug);

        IEnumerable<TagVm> GetTagByPostId(int postId);


    }
}
