﻿using AutoMapper;
using JustBlog.Data.Infrastructures;
using JustBlog.Models.Entities;
using JustBlog.ViewModels.Tags;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Application.Tags
{
    public class TagService : ITagService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TagService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public bool Create(TagVm tagVm)
        {
            try
            {
                var tag = new Tag()
                {
                    Id = tagVm.Id,
                    Name = tagVm.Name,
                    IsDeleted = tagVm.IsDeleted,
                    UrlSlug = tagVm.UrlSlug,
                    Decription = tagVm.Decription,
                    Count =tagVm.Count,
                    CreatedOn = tagVm.CreatedOn,
                    UpdatedOn = tagVm.UpdatedOn
                };
                var exsistTag=this.unitOfWork.TagRepository.Find(x => x.Name == tag.Name);
                if (exsistTag!=null)
                {
                    return false;
                }
                this.unitOfWork.TagRepository.Add(tag);
                this.unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't Create a new Tag");
            }
        }

        public bool Delete(int id)
        {
            try
            {
                this.unitOfWork.TagRepository.Delete(keyValues: id);
                this.unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IEnumerable<TagVm> GetAll(bool isDeleted = false)
        {
            var tags = this.unitOfWork.TagRepository.GetAll(isDeleted);
            var tagVms = new List<TagVm>();
            foreach (var item in tags)
            {
                var tagVm = new TagVm()
                {
                    Id = item.Id,
                    Name = item.Name,
                    IsDeleted = item.IsDeleted,
                    UrlSlug = item.UrlSlug,
                    Decription = item.Decription,
                    Count = item.Count,
                    CreatedOn = item.CreatedOn,
                    UpdatedOn = item.UpdatedOn
                };
                tagVms.Add(tagVm);
            }
            return tagVms;
        }

        public TagVm GetById(int id)
        {
            var tag = this.unitOfWork.TagRepository.GetById(id);

            var tagVm = new TagVm()
            {
                Id = tag.Id,
                Name = tag.Name,
                IsDeleted = tag.IsDeleted,
                UrlSlug = tag.UrlSlug,
                Decription = tag.Decription,
                Count = tag.Count,
                CreatedOn = tag.CreatedOn,
                UpdatedOn = tag.UpdatedOn
            };

            return tagVm;
        }

        public TagVm GetByUrlSlug(string urlSlug)
        {
            try
            {
                var tag=this.unitOfWork.TagRepository.GetTagByUrlSlug(urlSlug);
                var tagVm = new TagVm()
                {
                    Id = tag.Id,
                    Name = tag.Name,
                    IsDeleted = tag.IsDeleted,
                    UrlSlug = tag.UrlSlug,
                    Decription = tag.Decription,
                    Count = tag.Count,
                    CreatedOn = tag.CreatedOn,
                    UpdatedOn = tag.UpdatedOn
                };
                return tagVm;

            }
            catch (Exception)
            {

                throw new Exception("Not found tag");
            }
        }

        public IEnumerable<TagVm> GetTagByPostId(int postId)
        {
            // PostTagMaps
            var postTagMaps = this.unitOfWork.JustBlogDbContext
                                  .PostTagMaps.Where(pt => pt.PostId == postId);
            // get Tag by postTagMaps
            foreach (var postTag in postTagMaps)
            {
                var tag = this.unitOfWork.TagRepository.GetById(postTag.TagId);
                yield return this.mapper.Map<TagVm>(tag);
            }
        }

        public bool Update(TagVm tagVm)
        {
            try
            {
                var tag = this.unitOfWork.TagRepository.GetById(tagVm.Id);
                tag.Name = tagVm.Name;

                this.unitOfWork.TagRepository.Update(tag);
                this.unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("Can't update Tag");
            }
        }
    }
}