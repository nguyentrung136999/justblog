﻿using AutoMapper;
using JustBlog.Common.Constraints;
using JustBlog.Data.Infrastructures;
using JustBlog.Models.Entities;
using JustBlog.ViewModels;
using JustBlog.ViewModels.Posts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JustBlog.Application.Posts
{
    public class PostService : IPostService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public PostService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public bool Create(CreatePostVm createPostVm)
        {
            try
            {
                var postTagMaps = new List<PostTagMap>();
                // cần phải có tag => add tag.
                if (!string.IsNullOrEmpty(createPostVm.Tags))
                {
                    var tagIds = this.unitOfWork.TagRepository.AddTagByString(createPostVm.Tags);

                    foreach (var tagId in tagIds)
                    {
                        var postTagMap = new PostTagMap()
                        {
                            // Tag = this.unitOfWork.TagRepository.Find(x=>x.Id==tagId).FirstOrDefault(),
                            TagId = tagId
                        };
                        postTagMaps.Add(postTagMap);
                    }
                }
                // tạo PostTagMaps

                var post = new Post()
                {
                    Title = createPostVm.Title,
                    CategoryId = createPostVm.CategoryId,
                    ShortDesciption = createPostVm.ShortDesciption,
                    Content = createPostVm.Content,
                    UrlSlug = createPostVm.UrlSlug,
                    PostTagMaps = postTagMaps
                };

                this.unitOfWork.PostRepository.Add(post);
                return this.unitOfWork.SaveChanges() > 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                this.unitOfWork.PostRepository.Delete(false, id);
                return this.unitOfWork.SaveChanges() > 0;
            }
            catch (Exception)
            {
                throw new Exception("Delete failed");
            }
        }

        public IEnumerable<PostVm> GetAll(bool isDeleted = false)
        {
            try
            {
                var posts = this.unitOfWork.PostRepository.GetAll();
                List<PostVm> postVms = new List<PostVm>();
                foreach (var item in posts)
                {
                    var postVm=mapper.Map<PostVm>(item);
                    postVms.Add(postVm);
                }

                return postVms;
            }
            catch (Exception)
            {
                throw new Exception("Can't find post");
            }
        }

        public PostVm GetById(int id)
        {
            var postExsing = this.unitOfWork.PostRepository.GetById(id);
            return this.mapper.Map<PostVm>(postExsing);
        }

        public IEnumerable<PostVm> GetLastestPost(int size)
        {
            try
            {
                var posts = this.unitOfWork.PostRepository.GetLastestPost(size);
                List<PostVm> postVms = new List<PostVm>();
                foreach (var item in posts)
                {
                    var postVm = mapper.Map<PostVm>(item);
                    postVms.Add(postVm);
                   
                }

                return postVms;
            }
            catch (Exception)
            {
                throw new Exception("Can't find post");
            }
        }

        public bool Update(PostVm postVm)
        {
            try
            {
                //Delete PostTagMaps references Post
                var postTags = this.unitOfWork.JustBlogDbContext
                                    .PostTagMaps.Where(pt => pt.PostId == postVm.Id);
                this.unitOfWork.JustBlogDbContext.PostTagMaps.RemoveRange(postTags);

                this.unitOfWork.SaveChanges();
                // get existing post
                var postExisting = this.unitOfWork.PostRepository.GetById(postVm.Id);
                // tạo PostTagMaps

                var postTagMaps = new List<PostTagMap>();

                var tagIds = this.unitOfWork.TagRepository.AddTagByString(postVm.Tags);

                foreach (var tagId in tagIds)
                {
                    var postTagMap = new PostTagMap()
                    {
                        TagId = tagId
                    };
                    postTagMaps.Add(postTagMap);
                }

                postExisting.PostTagMaps = postTagMaps;
                postExisting.Title = postVm.Title;
                postExisting.UrlSlug = postVm.UrlSlug;
                postExisting.Content = postVm.Content;
                postExisting.ShortDesciption = postVm.ShortDesciption;
                this.unitOfWork.PostRepository.Update(postExisting);
                return this.unitOfWork.SaveChanges() > 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PagedVm<PostVm> GetPaging(Func<Post, bool> filter, string searchBy = Constraints.Search.Title, bool isDeleted = false, int pageSize = 10, int pageIndex = 1, string typeOfSoft = "ASC")
        {
            Func<IEnumerable<Post>, IOrderedEnumerable<Post>> order = null;

            if (typeOfSoft == Constraints.Soft.ASC)
            {
                switch (searchBy)
                {
                    case Constraints.Search.Title:
                        order = x => x.OrderBy(x => x.Title);
                        break;

                    case Constraints.Search.Content:
                        order = x => x.OrderBy(x => x.Content);
                        break;
                }
            }
            else
            {
                switch (searchBy)
                {
                    case Constraints.Search.Title:
                        order = x => x.OrderByDescending(x => x.Title);
                        break;

                    case Constraints.Search.Content:
                        order = x => x.OrderByDescending(x => x.Content);
                        break;
                }
            }

            var posts = this.unitOfWork.PostRepository
                      .GetPaging(filter: filter, orderBy: order, isDeleted: isDeleted, pageSize: pageSize, pageIndex: pageIndex);

            var postVms = this.mapper.Map<IEnumerable<PostVm>>(posts.Data);

            return new PagedVm<PostVm>(postVms, pageIndex, pageSize, posts.TotalPage);
        }

        public IEnumerable<PostVm> GetPublishPost()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PostVm> GetUnPublishPost()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PostVm> GetPostByTag()
        {
            throw new NotImplementedException();
        }
    }
}