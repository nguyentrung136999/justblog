﻿using JustBlog.Common.Constraints;
using JustBlog.Models.Entities;
using JustBlog.ViewModels;
using JustBlog.ViewModels.Posts;
using System;
using System.Collections.Generic;

namespace JustBlog.Application.Posts
{
    public interface IPostService
    {
        IEnumerable<PostVm> GetAll(bool isDeleted = false);

        bool Create(CreatePostVm createPostVm);

        PostVm GetById(int id);

        bool Update(PostVm postVm);

        bool Delete(int id);
        IEnumerable<PostVm> GetLastestPost(int size);

        IEnumerable<PostVm> GetPublishPost();

        IEnumerable<PostVm> GetUnPublishPost();

        IEnumerable<PostVm> GetPostByTag();

        PagedVm<PostVm> GetPaging(Func<Post, bool> filter, string searchBy = Constraints.Search.Title,
            bool isDeleted = false, int pageSize = 10, int pageIndex = 1,
            string typeOfSoft = Constraints.Soft.ASC);

    }
}