﻿using JustBlog.Models.BaseEntity;
using System.Collections.Generic;

namespace JustBlog.Models.Entities
{
    public class Post : EntityBase<int>
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public string ShortDesciption { get; set; }

        public string UrlSlug { get; set; }

        public int CategoryId { get; set; }

        public Category Category { get; set; }

        public ICollection<PostTagMap> PostTagMaps { get; set; }

        public int ViewCount { get; set; }

        public int RateCount { get; set; }

        public int TotalRate { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}