﻿using JustBlog.Models.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Models.Entities
{
    public class Comment : EntityBase<int>
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public int PostId { get; set; }

        public Post Post { get; set; }

        public string CommentHeader { get; set; }

        public string CommentText { get; set; }

        public DateTime CommentTime { get; set; }

    }
}
