﻿using JustBlog.Models.BaseEntity;

namespace JustBlog.Models.Entities
{
    public class PostTagMap:EntityBase<int>
    {
        public int PostId { get; set; }

        public int TagId { get; set; }

        public Post Post { get; set; }

        public Tag Tag { get; set; }
    }
}