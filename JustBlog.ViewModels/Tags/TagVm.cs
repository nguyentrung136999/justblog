﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace JustBlog.ViewModels.Tags
{
    public class TagVm:BaseEntity
    {
        
        [StringLength(50, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 2)]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Name { get; set; }

        [DisplayName("Link Url")]
        [StringLength(50, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 2)]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string UrlSlug { get; set; }

        public string Decription { get; set; }
        public int Count { get; set; }
    }
}
