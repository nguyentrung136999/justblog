﻿using JustBlog.Models.Entities;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace JustBlog.ViewModels.Posts
{
    public class PostVm : BaseEntity
    {
        [StringLength(50, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 2)]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string Title { get; set; }

        public int CategoryId { get; set; }

        [Required(ErrorMessage = "{0} không được để trống")]
        public string Content { get; set; }

        [StringLength(250, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 2)]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string ShortDesciption { get; set; }

        [Required]
        public string UrlSlug { get; set; }

        public string Tags { get; set; }
    }
}