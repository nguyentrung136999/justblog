﻿using AutoMapper;
using JustBlog.Models.Entities;
using JustBlog.ViewModels.Posts;
using JustBlog.ViewModels.Tags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.ViewModels.AutoMappers
{
   public class MappingProfile:Profile
    {
        //titile1
        public MappingProfile()
        {
            CreateMap<Post, PostVm>()
                .ForMember(pv => pv.Id, option => option.MapFrom(p => p.Id))
                .ForMember(pv => pv.Title, option => option.MapFrom(p => p.Title))
                .ForMember(pv => pv.UrlSlug, option => option.MapFrom(p => p.UrlSlug))
                .ForMember(pv => pv.CategoryId, option => option.MapFrom(p => p.CategoryId))
                .ForMember(pv => pv.UpdatedOn, option => option.MapFrom(p => p.UpdatedOn))
                .ForMember(pv => pv.CreatedOn, option => option.MapFrom(p => p.CreatedOn))
                .ReverseMap();

            CreateMap<Tag, TagVm>().ReverseMap();
        }
    }
}
