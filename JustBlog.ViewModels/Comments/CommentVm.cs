﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.ViewModels.Comments
{
    public  class CommentVm:BaseEntity
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public int PostId { get; set; }

        public string CommentHeader { get; set; }

        public string CommentText { get; set; }

        public DateTime CommentTime { get; set; }
    }
}
